## Name
bc_rstudio_singularity

## Description
[Open OnDemand](https://openondemand.org/) Batch Connect application that will be used to launch RStudio Server from inside various Singularity images. We will have a library of images that will have various R packages in addition to RStudio Server that a user can choose from.

## Installation
This project can be installed by a user that has developer rights in their OnDemand installation by cloning underneath ~/ondemand/dev/. Administrators can install for all users by cloning under /var/www/ood/apps/sys. Remember to change the cluster name in form.yml.erb.

## Support
Visit [Duke Compute Cluster Open OnDemand](https://dcc.duke.edu/OpenOnDemand/) for further information.

## Authors and acknowledgment
This project is based upon the work at [New Mexico State University's High Performance Computing Group](https://hpc.nmsu.edu/) specifically the project [ood_bc_rstudio](https://gitlab.com/nmsu_hpc/ood_bc_rstudio).

## License
MIT License
